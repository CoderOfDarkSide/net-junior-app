namespace JuniorTestApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class new_field : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SKU = c.String(nullable: false),
                        Brand = c.String(nullable: false),
                        Price = c.Double(nullable: false),
                        Weight = c.Int(nullable: true),
                        Feature = c.String(),
                        ProductParameter = c.String(),
                        ColumnNames = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Items");
        }
    }
}
