﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace JuniorTestApp.Models
{
    public class Item
    {
        public int Id { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string SKU { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Brand { get; set; }
        
        [Range(0, Single.MaxValue)]
        public Single? Price { get; set; }
        [Range(0, Single.MaxValue)]
        public Single? Weight { get; set; }
        public string Feature { get; set; }
        public string ProductParameter { get; set; }
        public string ColumnNames { get; set; }
    }

    public class ItemContext : DbContext
    {
        public DbSet<Item> Items { get; set; }
    }
}