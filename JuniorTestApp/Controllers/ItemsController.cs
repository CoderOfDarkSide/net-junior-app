﻿using JuniorTestApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Data.SqlClient;
using PagedList;
using System.Data.Entity.Validation;
using System.Globalization;

namespace JuniorTestApp.Controllers
{
    public class ItemsController : Controller
    {
        ItemContext db = new ItemContext();

        // GET: Items
        public ActionResult Index(int? page)
        {
            int pageSize = 50;
            int pageNumber = (page ?? 1);
            return View(db.Items.OrderBy(i => i.SKU).ToPagedList(pageNumber, pageSize));
        }

        // POST: Items/Import
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase csvFile)
        {
            string filePath = String.Empty;
            if (csvFile != null)
            {
                string pathToDir = Server.MapPath("~/Uploads/");
                if(!Directory.Exists(pathToDir))
                {
                    Directory.CreateDirectory(pathToDir);
                }

                filePath = pathToDir + Path.GetFileName(csvFile.FileName);
                csvFile.SaveAs(filePath);

                string csvData = System.IO.File.ReadAllText(filePath);
                ViewBag.csvData = csvData;
                return View();
            }
            Debug.WriteLine("Csv file is null!");
            return View();
        }

        // POST: Items/Mapping
        [HttpPost]
        public ActionResult Mapping(string csvData, FormCollection collection)
        {
            ItemContext itemContex = new ItemContext();
            List<Item> items = new List<Item>();

            string[] columns = collection[2].Split(',');
            string[] parameters = collection[3].Split(',');
            
            if (columns.Length != parameters.Length)
            {
                string error = columns.Length.ToString() + " " + parameters.Length.ToString();

                return Redirect("/Index");
            }            

            foreach (string row in csvData.Split('\n').Skip(1))
            {
                if(!String.IsNullOrEmpty(row))
                {
                    Item item = new Item();
                    string features = String.Empty;
                    string productParameters = String.Empty;
                    string columnNames = String.Empty;

                    for (int i = 0; i < row.Split(',').Count(); i++)
                    {
                        string field = row.Split(',').ElementAt(i);

                        if (parameters[i] != "Ignore")
                        {
                            columnNames += columns[i] + "|";
                            if(!String.IsNullOrEmpty(field))
                            {
                                switch (parameters[i])
                                {
                                    case "SKU":
                                        try
                                        {
                                            item.SKU = field;
                                        }
                                        catch (Exception ex)
                                        {
                                            Debug.Write(ex.Message + "\n");
                                        }
                                        break;
                                    case "Brand":
                                        try
                                        {
                                            item.Brand = field;
                                        }
                                        catch (Exception ex)
                                        {
                                            Debug.Write(ex.Message + "\n");
                                        }
                                        break;
                                    case "Price":
                                        try
                                        {
                                            item.Price = float.Parse(field, CultureInfo.InvariantCulture.NumberFormat);
                                        }
                                        catch (Exception ex)
                                        {
                                            Debug.Write(ex.Message + "\n");
                                        }
                                        break;
                                    case "Weight":
                                        try
                                        {
                                            item.Weight = float.Parse(field, CultureInfo.InvariantCulture.NumberFormat);
                                        }
                                        catch (Exception ex)
                                        {
                                            Debug.Write(ex.Message + "\n");
                                        }
                                        break;
                                    case "Feature":
                                        features += field + "|";
                                        break;
                                    case "Product parameter":
                                        productParameters += field + "|";
                                        break;
                                    default:
                                        Debug.Write(parameters[i] + "\n");
                                        break;
                                }
                            }
                        }
                    }

                    item.ColumnNames = columnNames;
                    item.Feature = features;
                    item.ProductParameter = productParameters;

                    itemContex.Items.Add(item);
                }
            }

            try
            {
                itemContex.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                foreach (DbEntityValidationResult validationError in ex.EntityValidationErrors)
                {
                    Response.Write("Object: " + validationError.Entry.Entity.ToString());
                    Response.Write("");
                    foreach (DbValidationError err in validationError.ValidationErrors)
                    {
                        Response.Write(err.ErrorMessage + "");
                    }
                }
            }

            return RedirectToAction("Index");
        }
        
    }
}
